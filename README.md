# Climb3D

Simple project to learn modern OpenGL, still in a very early stage. For now, it's just a terrain with some cows and Steve.

<img src="readme_image.png" width="550px">

### Dependencies

- GCC - GNU Compiler Collection
- GLFW - framework to create/manage windows and OpenGL contexts
- GLM - C++ math library for 3D software based on GLSL specification

### References

*Learn OpenGL*<br/>
https://learnopengl.com/

*Coding Minecraft in One Week - C++/OpenGL Programming Challenge*<br/>
https://www.youtube.com/watch?v=Xq3isov6mZ8&t=927s
