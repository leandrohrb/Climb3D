#ifndef USER_INPUTS_H
#define USER_INPUTS_H

/* Functions to deal with user inputs */

#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

/* Deal with user WASD movement (returns displacement) */
glm::vec3 userWASDMovement(GLFWwindow *window, float delta_time,
    glm::vec3 camera_pos, glm::vec3 camera_front, glm::vec3 camera_up);

/* Deal with mouse movement */
glm::vec3 userMouseMovement(char *first_mouse, float *last_x, float *last_y,
    double x_pos, double y_pos, float *yaw, float *pitch);

#endif
