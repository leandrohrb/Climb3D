#ifndef MAP_GEN_H
#define MAP_GEN_H

/* Functions to create map from perling noise */

#include <stdio.h>
#include <stdlib.h>


/* Receives map width and depth and generate random map */
int **height_map(int x_size, int z_size);

#endif
