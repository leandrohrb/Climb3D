#ifndef SHADERS_H
#define SHADERS_H

/* Functions to read shaders from source and compile them */

#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>

/* Receives vertex and fragment shaders source path; return shader program */
unsigned int createShaderProgram(const char *vertex_shader_path,
                                 const char *fragment_shader_path);

/* Find out shader source code size */
int sourceCodeSize(FILE * file);

/* Read shader source code and compile it */
unsigned int compileShader(const char *file_path, GLenum shader_type);

/* Creates shader program, attach shaders to it and link them */
unsigned int linkShaders(unsigned int shader0, unsigned int shader1);

#endif
