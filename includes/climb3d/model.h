#ifndef MODEL_H
#define MODEL_H

/* Functions to load models using Assimp */

#include <glm/glm.hpp>
#include <glad/glad.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <string.h>

struct Vertex {
    glm::vec3 pos;
    glm::vec3 normal;
    glm::vec2 text_coords;
};

struct Texture {
    unsigned int id;
    const char *type;
    const char *path;
};

struct Mesh {
    Vertex *vertices;
    unsigned int *indices;
    Texture *textures;
    unsigned int n_vertices, n_indices, n_textures;
    unsigned int VAO, VBO, EBO;
};

struct Skybox {
    float *vertices;
    unsigned int texture_id, VAO, VBO, EBO;
};

/* Load skybox */
Skybox *loadSkybox();

/* Load model and process it's meshes */
Mesh *loadModel(char *path, unsigned int *n_meshes);

/* Process node's meshes and then its sons */
void processModelNode(aiNode *node, unsigned int *mesh_free_pos,
                      const aiScene *scene, Mesh *meshes, char *directory);

/* Initialize our mesh with mesh loaded with Assimp library */
void initMesh(Mesh *our_mesh, aiMesh *assimp_mesh, const aiScene *scene,
              char *directory);

/* Setup mesh so it can be draw after */
void setupMesh(Mesh *mesh);

#endif
