#ifndef DRAW_H
#define DRAW_H

/* Functions to draw loaded models and other stuff */

#include <climb3d/model.h>

/* Draw an object */
void drawObject(unsigned int shader_program, Mesh *meshes,
                unsigned int n_meshes);

/* Draw skybox */
void drawSkybox(Skybox *skybox);

#endif                
