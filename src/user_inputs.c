#include <climb3d/user_inputs.h>


/* Deal with user WASD movement (returns displacement) */
glm::vec3 userWASDMovement(GLFWwindow *window, float delta_time,
    glm::vec3 camera_pos, glm::vec3 camera_front, glm::vec3 camera_up)
{
    float camera_speed = 2.0f * delta_time;

    glm::vec3 camera_right = glm::normalize(
        glm::cross(camera_front, camera_up)
    );

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        return (camera_speed * camera_front);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        return (- camera_speed * camera_front);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        return (- camera_speed * camera_right);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        return (camera_speed * camera_right);

    return (glm::vec3(0.0f, 0.0f, 0.0f));
}


/* Deal with mouse movement */
glm::vec3 userMouseMovement(char *first_mouse, float *last_x, float *last_y,
    double x_pos, double y_pos, float *yaw, float *pitch)
{    
    if (*first_mouse)
    {
        (*first_mouse) = 0;
        (*last_x) = x_pos;
        (*last_y) = y_pos;
    }
   
    float sensitivity = 0.1f;
    float x_offset = sensitivity * (x_pos - (*last_x));
    float y_offset = sensitivity * ((*last_y) - y_pos);

    (*last_x) = x_pos;
    (*last_y) = y_pos;

    (*yaw) += x_offset;
    (*pitch) += y_offset;

    if ((*pitch) > 89.0f)
        (*pitch) = 89.0f;
    else if ((*pitch) < -89.0f)
        (*pitch) = -89.0f;

    glm::vec3 front;
    front.x = cos(glm::radians(*yaw)) * cos(glm::radians(*pitch));
    front.y = sin(glm::radians(*pitch));
    front.z = sin(glm::radians(*yaw)) * cos(glm::radians(*pitch));

    return (glm::normalize(front));
}
