/* Functions to read shaders from source and compile them */

#include <climb3d/shaders.h>


/* Receives vertex and fragment shaders source path; return shader program */
unsigned int createShaderProgram(const char *vertex_shader_path,
                                 const char *fragment_shader_path)
{    
    // compile vertex and fragment shaders
    unsigned int vertex_shader = compileShader(vertex_shader_path,
                                               GL_VERTEX_SHADER);
    unsigned int fragment_shader = compileShader(fragment_shader_path,
                                    GL_FRAGMENT_SHADER);

    // link them into a shader program                                    
    unsigned int shader_program = linkShaders(vertex_shader, fragment_shader);

    // they aren't necessary anymore
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    return (shader_program);
}


/* Find out shader source code size (bytes) */
int sourceSize(FILE *file)
{
    fseek(file, 0L, SEEK_END);
    int size = ftell(file);
    rewind(file);
    return(size);
}


/* Read shader source code and compile it */
unsigned int compileShader(const char *file_path, GLenum shader_type)
{
    // allocate memory for string
    FILE *source_file = fopen(file_path, "r");
    int file_size = sourceSize(source_file);
    char *source = (char*) malloc(file_size + 1);

    // read file as string
    fread(source, 1, file_size, source_file);
    source[file_size] = '\0';
    fclose(source_file);

    // create shader object
    unsigned int shader;
    shader = glCreateShader(shader_type);

    // attach the source code to the object and then compile it
    glShaderSource(shader, 1, &source, NULL);
    free(source);
    glCompileShader(shader);

    // check for errors
    int success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    // no errors
    if (success)
        return (shader);

    // errors
    char error_log[512];
    glGetShaderInfoLog(shader, 512, NULL, error_log);

    if (shader_type == GL_VERTEX_SHADER)
        printf("%s", "Error: vertex shader compilation failed\n");
    else if (shader_type == GL_FRAGMENT_SHADER)
        printf("%s", "Error: fragment shader compilation failed\n");
    printf("%s", error_log);

    return 0;
}


/* Creates shader program, attach shaders to it and link them */
unsigned int linkShaders(unsigned int shader0, unsigned int shader1)
{
    unsigned int shader_program = glCreateProgram();

    // attach shaders to program and link them
    glAttachShader(shader_program, shader0);
    glAttachShader(shader_program, shader1);
    glLinkProgram(shader_program);

    // check for errors
    int success;
    glGetShaderiv(shader_program, GL_LINK_STATUS, &success);

    // no errors
    if (success)
        return (shader_program);

    // errors
    char error_log[512];
    glGetShaderInfoLog(shader_program, 512, NULL, error_log);

    printf("%s", "Error: shader program link process failed\n");
    printf("%s", error_log);

    return 0;
}
