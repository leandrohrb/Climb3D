/* Functions to load models using Assimp */

#include <climb3d/model.h>
#include <stb_image.h>


/* Set skybox vertices */
void setSkyboxVertices(Skybox *skybox)
{
    float skybox_vertices[] =
    {
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f,  1.0f, -1.0f
    };

    int skybox_indices[] =
    {
        0, 1, 2, 0, 2, 3, // front
        7, 6, 5, 7, 5, 4, // back
        0, 3, 7, 0, 7, 4, // up
        1, 2, 6, 1, 6, 5, // down
        3, 2, 6, 3, 6, 7, // right
        4, 5, 1, 4, 1, 0  // left
    };

    glGenVertexArrays(1, &(skybox->VAO));
    glGenBuffers(1, &(skybox->VBO));
    glGenBuffers(1, &(skybox->EBO));

    glBindVertexArray(skybox->VAO);

    glBindBuffer(GL_ARRAY_BUFFER, skybox->VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skybox_vertices),
                 &skybox_vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, skybox->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(skybox_indices),
                 &skybox_indices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


/* Load skybox */
Skybox *loadSkybox()
{
    Skybox *skybox = (Skybox *) malloc(sizeof(Skybox));
    unsigned int i;
    int width, height, n_channels;

    // TODO: at least receive directory path;
    // right, left, etc can be hardcoded
    const char *faces[] = {
        "../assets/skybox/space_ft.png", // front
        "../assets/skybox/space_bk.png", // back
        "../assets/skybox/space_up.png", // up
        "../assets/skybox/space_dn.png", // down
        "../assets/skybox/space_rt.png", // right
        "../assets/skybox/space_lf.png"  // left
    };

    // create texture and bind it
    glGenTextures(1, &(skybox->texture_id));
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox->texture_id);

    // load all the faces of the skybox image and
    // map them to a cubemap texture
    stbi_set_flip_vertically_on_load(false);
    for (i = 0; i < 6; i++)
    {
        unsigned char *data = stbi_load(faces[i], &width,
                                        &height, &n_channels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB,
                         width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }
        else
        {
            stbi_image_free(data);
            printf("FAILED TO LOAD IMAGE\n");
        }   
    }
    stbi_set_flip_vertically_on_load(true);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    // set skybox vertices
    setSkyboxVertices(skybox);
    
    return (skybox);
}


/* Load texture from file path */
unsigned int loadTexture(const char *filename, char *directory)
{
    unsigned int texture_id;
    glGenTextures(1, &texture_id);

    // texture image path
    char *path = (char *) malloc(
        (strlen(directory)+strlen(filename)+1) * sizeof(char)
    );
    strcpy(path, directory);
    strcat(path, filename);

    int width, height, n_components;
    unsigned char *data = stbi_load(path, &width, &height, &n_components, 0);

    if (data)
    {
        GLenum format;
        switch(n_components)
        {
            case(1):
                format = GL_RED;
                break;
            case(3):
                format = GL_RGB;
                break;
            case(4):
                format = GL_RGBA;
                break;                
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glBindTexture(GL_TEXTURE_2D, texture_id);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format,
                     GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        stbi_image_free(data);
    }
    else
    {
        stbi_image_free(data);
        printf("FAILED TO LOAD IMAGE\n");
    }

    return texture_id;
}


/* Count total number of meshes (sum of each node) */
unsigned int nMeshes(aiNode *node)
{
    unsigned int i;
    unsigned int n_meshes = 0;

    for (i = 0; i < node->mNumChildren; i++)
        n_meshes += nMeshes(node->mChildren[i]);
    n_meshes += node->mNumMeshes;

    return n_meshes;
}


/* Load model and process it's meshes */
Mesh *loadModel(char *path, unsigned int *n_meshes)
{
    // load model and check if errors ocurred
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile(path, aiProcess_Triangulate);
    if (!scene || !scene->mRootNode ||
        scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE)
    {
        printf("%s", "Error loading model\n");
        return (NULL);
    }

    // get directory from texture object path
    long unsigned int dir_path_size = sizeof(char) * 
                                      (strrchr(path, '/') - path);
    char *directory = (char *) malloc(dir_path_size + 1);
    memcpy(directory, path, dir_path_size);
    directory[dir_path_size] = '/';
    directory[dir_path_size + 1] = '\0';

    // total number of meshes
    (*n_meshes) = nMeshes(scene->mRootNode);

    // allocate memory for all of them
    Mesh *meshes = (Mesh *) malloc(
        (*n_meshes) * sizeof(Mesh)
    );

    // process each node recursively (starting with root)
    unsigned int mesh_free_pos = 0;
    processModelNode(scene->mRootNode, &mesh_free_pos, scene,
        meshes, directory);

    // setup all meshes
    unsigned int i;
    for (i = 0; i < (*n_meshes); i++)
        setupMesh(&meshes[i]);

    return(meshes);
}


/* Process node's meshes and then its sons */
void processModelNode(aiNode *node, unsigned int *mesh_free_pos,
                      const aiScene *scene, Mesh *meshes, char *directory)
{
    unsigned int i;

    // process every node's meshes
    for (i = 0 ; i < node->mNumMeshes; i++)
    {
        initMesh(&meshes[*mesh_free_pos], scene->mMeshes[node->mMeshes[i]],
                 scene, directory);
        *mesh_free_pos += 1;
    }

    // process every node's child
    for (i = 0; i < node->mNumChildren; i++)
        processModelNode(node->mChildren[i], mesh_free_pos, scene,
                         meshes, directory);
}


/* Initialize our mesh with mesh loaded with Assimp library */
void initMesh(Mesh *our_mesh, aiMesh *assimp_mesh, const aiScene *scene,
              char *directory)
{
    unsigned int i, j, k;
    unsigned int n_indices, n_faces;

    // allocate memory for vertices and fill them
    our_mesh->n_vertices = assimp_mesh->mNumVertices;
    our_mesh->vertices = (Vertex *) malloc(
        our_mesh->n_vertices * sizeof(Vertex)
    );

    for (i = 0; i < assimp_mesh->mNumVertices; i++)
    {
        // vertices position data
        our_mesh->vertices[i].pos.x = assimp_mesh->mVertices[i].x;
        our_mesh->vertices[i].pos.y = assimp_mesh->mVertices[i].y;
        our_mesh->vertices[i].pos.z = assimp_mesh->mVertices[i].z;

        // vertices normal data
        our_mesh->vertices[i].normal.x = assimp_mesh->mNormals[i].x;
        our_mesh->vertices[i].normal.y = assimp_mesh->mNormals[i].y;
        our_mesh->vertices[i].normal.z = assimp_mesh->mNormals[i].z;

        // vertices text coord data (usually only interested in the first)
        if (assimp_mesh->mTextureCoords[0])
        {
            our_mesh->vertices[i].text_coords.x =
                assimp_mesh->mTextureCoords[0][i].x;
            our_mesh->vertices[i].text_coords.y =
                assimp_mesh->mTextureCoords[0][i].y;
        }
        else
            our_mesh->vertices[i].text_coords = glm::vec2(0.0f, 0.0f);
    }

    // alocate memory for indices ...
    our_mesh->n_indices = 0;
    n_faces = assimp_mesh->mNumFaces;
    for (i = 0; i < n_faces; i++)
        our_mesh->n_indices += assimp_mesh->mFaces[i].mNumIndices;

    our_mesh->indices = (unsigned int *) malloc(
        our_mesh->n_indices * sizeof(unsigned int)
    );

    // ... and fill them with indices data of each face of the mesh
    n_indices = 0;
    for (i = 0; i < n_faces; i++)
    {
        for (j = 0; j < assimp_mesh->mFaces[i].mNumIndices; j++)
            our_mesh->indices[n_indices + j] = assimp_mesh->mFaces[i].mIndices[j];

        n_indices += assimp_mesh->mFaces[i].mNumIndices;
    }

    // allocate memory for texture objects and fill them
    if (assimp_mesh->mMaterialIndex >= 0)
    {
        aiMaterial *material = scene->mMaterials[assimp_mesh->mMaterialIndex];

        // compute number of texture objects
        our_mesh->n_textures =
            material->GetTextureCount(aiTextureType_DIFFUSE) +
            material->GetTextureCount(aiTextureType_SPECULAR) +
            material->GetTextureCount(aiTextureType_AMBIENT);

        // allocate memory for all texture objects
        our_mesh->textures = (Texture *) malloc(
            our_mesh->n_textures * sizeof(Texture)
        );

        // store DIFFUSE texture objects
        for (i = 0; i < material->GetTextureCount(aiTextureType_DIFFUSE); i++)
        {
            aiString str;
            material->GetTexture(aiTextureType_DIFFUSE, i, &str);
            our_mesh->textures[i].path = str.C_Str();
            our_mesh->textures[i].type = "diffuse";
            our_mesh->textures[i].id = loadTexture(str.C_Str(), directory);
        }

        // store SPECULAR texture objects
        for (j = 0; j < material->GetTextureCount(aiTextureType_SPECULAR); j++)
        {
            aiString str;
            material->GetTexture(aiTextureType_SPECULAR, j, &str);
            our_mesh->textures[i + j].path = str.C_Str();
            our_mesh->textures[i + j].type = "specular";
            our_mesh->textures[i + j].id = loadTexture(str.C_Str(), directory);
        }

        // store AMBIENT texture objects
        for (k = 0; k < material->GetTextureCount(aiTextureType_AMBIENT); k++)
        {
            aiString str;
            material->GetTexture(aiTextureType_AMBIENT, k, &str);
            our_mesh->textures[i + j + k].path = str.C_Str();
            our_mesh->textures[i + j + k].type = "ambient";
            our_mesh->textures[i + j + k].id = loadTexture(str.C_Str(), directory);
        }
    }
}


/* Setup mesh so it can be draw after */
void setupMesh(Mesh *mesh)
{
    glGenVertexArrays(1, &(mesh->VAO));
    glGenBuffers(1, &(mesh->VBO));
    glGenBuffers(1, &(mesh->EBO));

    glBindVertexArray(mesh->VAO);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->VBO);
    glBufferData(GL_ARRAY_BUFFER, mesh->n_vertices * sizeof(Vertex), 
        &(mesh->vertices[0]), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 mesh->n_indices * sizeof(unsigned int),
                 &(mesh->indices[0]), GL_STATIC_DRAW);

    // vertex posistions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    // vertex normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex), (void*)(3*sizeof(float)));
    // vertex texture coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex), (void*)(6*sizeof(float)));

    glBindVertexArray(0);
}

