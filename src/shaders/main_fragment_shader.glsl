#version 330 core

in vec2 TexCoords;
in vec3 Normal;
in vec3 FragPos;

out vec4 FragColor;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_ambient1;

uniform vec3 cameraPos;


void main()
{
    vec3 light_color = vec3(1.0f, 1.0f, 1.0f);

    // ambient light
    vec3 ambient_light = 0.8f * light_color * texture(texture_ambient1, TexCoords).rgb;

    // diffuse light
    vec3 diffuse_light_norm = normalize(Normal);
    vec3 diffuse_light_pos = vec3(2.0f, 2.0f, 2.0f);
    vec3 diffuse_light_dir = normalize(diffuse_light_pos - FragPos);
    float diff = max(0.0f, dot(diffuse_light_norm, diffuse_light_dir));
    vec3 diffuse_light = 1.0f * diff * light_color * texture(texture_diffuse1, TexCoords).rgb;

    // specular light
    float specular_strength = 0.5f;
    vec3 view_dir = normalize(cameraPos - FragPos);
    vec3 reflect_dir = reflect(-diffuse_light_dir, diffuse_light_norm);
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0f), 32);
    vec3 specular_light = specular_strength * spec *  texture(texture_specular1, TexCoords).rgb;

    FragColor = vec4(ambient_light + diffuse_light + specular_light, 1.0f);
}
