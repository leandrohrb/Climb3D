/* ~~~ Headers ~~~ */

#include <math.h>
#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <string>

// GLM (OpenGL Mathematics library)
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// stb image
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

// glad header
#include <glad/glad.h>

// glfw header
#include <GLFW/glfw3.h>

#include <climb3d/shaders.h>
#include <climb3d/model.h>
#include <climb3d/draw.h>
#include <climb3d/user_inputs.h>
//#include <climb3d/read_map.h>
#include <climb3d/map_gen.h>


/* ~~~ Constants ~~~ */

const unsigned int screen_width = 800;
const unsigned int screen_height = 600;
const char *main_vertex_shader_path  = "shaders/main_vertex_shader.glsl";
const char *main_fragment_shader_path = "shaders/main_fragment_shader.glsl";
const char *skybox_vertex_shader_path  = "shaders/skybox_vertex_shader.glsl";
const char *skybox_fragment_shader_path = "shaders/skybox_fragment_shader.glsl";
const char *csv_map_path = "map.csv";
char minecraft_block_path[] = "../assets/minecraft_block/model.obj";
char minecraft_tree_path[] = "../assets/minecraft_tree/minecraft_tree.obj";
char minecraft_cow_path[] = "../assets/cow/untitled.obj";
char steve_path[] = "../assets/steve/steve.obj";


/* ~~~ Global variables ~~~ */

glm::vec3 camera_pos = glm::vec3(0.0f, 2.0f, 5.0f);
glm::vec3 camera_front = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 camera_up  = glm::vec3(0.0f, 1.0f,  0.0f);

float yaw = -90.0f;
float pitch = 0.0f;
float fov = 45.0f;
float last_x = screen_width / 2.0;
float last_y = screen_height / 2.0;
char first_mouse = 1;

float delta_time = 0.0f;
float last_frame = 0.0f;
float current_frame = 0.0f;

Mesh *minecraft_block; unsigned int minecraft_block_n_meshes;
Mesh *steve; unsigned int steve_n_meshes;
Mesh *minecraft_tree; unsigned int minecraft_tree_n_meshes;
Mesh *minecraft_cow; unsigned int minecraft_cow_n_meshes;

Skybox *skybox;


/* ~~~ Aux Functions ~~~ */

void framebufferSizeCallback(GLFWwindow *window, int width, int height);
void keyboardInput(GLFWwindow *window, float delta_time);
void mouseInputCallback(GLFWwindow* window, double x_pos, double y_pos);
void read_csv_map(FILE *map_file, unsigned int shader_program);
void setMat4Uniform(unsigned int shader_program, const char *var_name,
                    glm::mat4 value);
void setVec3Uniform(unsigned int shader_program, const char *var_name,
                    glm::vec3 value);



/* ~~~ Main function ~~~ */
int main()
{
    int x_pos, y_pos, z_pos;
    unsigned int i;

    // config stb_image.h to flip the y axis
    stbi_set_flip_vertically_on_load(true);

    // init glfw and configure for opengl 3.3 core
    glfwInit();
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // creates the main window using glfw
    GLFWwindow *window = glfwCreateWindow(screen_width, screen_height,
        "Hello, World!", NULL, NULL);
    if (window == NULL)
    {
        printf("%s", "Error: failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // initialize opengl using glad; glfwGetProcAddress finds the address of
    // the opengl functions in runtime (it's OS specific)
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        printf("%s", "Error: failed to initialize GLAD\n");
        return -1;
    }

    // tells opengl to render in this dimensions (the same of our glfw window)
    glViewport(0, 0, screen_width, screen_height);

    // enable opengl depth test
    glEnable(GL_DEPTH_TEST);

    // enable opengl multisampling
    glEnable(GL_MULTISAMPLE);

    // enable transparency
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // when user resizes the window, viewport has to be resized as well
    // callback function called when user resizes glfw window
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    // callback function when user move the mouse
    glfwSetCursorPosCallback(window, mouseInputCallback);
    // disable cursor to make 3d camera work
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // compile main vertex and fragment shaders and link them
    unsigned int main_shader_program = createShaderProgram(
        main_vertex_shader_path, main_fragment_shader_path);

    // compile skybox vertex and framgent shaders and link them
    unsigned int skybox_shader_program = createShaderProgram(
        skybox_vertex_shader_path, skybox_fragment_shader_path);

    // open csv map
    FILE *map_file = fopen(csv_map_path, "r");
    if (!map_file) {
        printf("Can't open file\n");
        return 0;
    }


    int width = 50;
    int depth = 50;
    int **map = height_map(width, depth);

    for (int i=0; i<width; i++)
        for (int j=0; j<depth; j++)
            printf("%d\n", map[i][j]);


    // load minecraft block
    minecraft_block = loadModel(minecraft_block_path,
                                &minecraft_block_n_meshes);
    // load steve
    steve = loadModel(steve_path, &steve_n_meshes);

    // load minecraft tree
    minecraft_tree = loadModel(minecraft_tree_path, &minecraft_tree_n_meshes);

    // load cow
    minecraft_cow = loadModel(minecraft_cow_path, &minecraft_cow_n_meshes);

    // load skybox
    skybox = loadSkybox();

    // main loop
    while (!glfwWindowShouldClose(window))
    {
        // delta time
        current_frame = glfwGetTime();
        delta_time = current_frame - last_frame;
        last_frame = current_frame;

        // poll user keyboard inputs and deal with them
        glfwPollEvents();
        keyboardInput(window, delta_time);

        // clear last buffer image and depth buffer
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate main shader program
        glUseProgram(main_shader_program);

        // update camera pos
        setVec3Uniform(main_shader_program, "cameraPos", camera_pos);

        // perspectice matrices
        glm::mat4 model, view, projection;
        glm::vec3 translation;

        // set view and projection matrices
        view = glm::lookAt(camera_pos, camera_pos + camera_front, camera_up);
        projection = glm::perspective(glm::radians(45.0f),
            screen_width/(float)screen_height, 0.1f, 100.0f);
        setMat4Uniform(main_shader_program, "view", view);
        setMat4Uniform(main_shader_program, "projection", projection);

        // read and draw game map
        //read_csv_map(map_file, main_shader_program);
        // read csv and draw map
        for (x_pos = 0; x_pos < width; x_pos++) {
            for (z_pos = 0; z_pos < depth; z_pos++) {

                    for (y_pos = 0; y_pos < map[x_pos][z_pos]; y_pos++) {
                        model = glm::mat4(1.0f);
                        model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
                        model = glm::translate(model,
                            2.0f * glm::vec3(0.0f + x_pos, 0.0f + y_pos, 0.0f + z_pos));
                        glUniformMatrix4fv(glGetUniformLocation(main_shader_program,
                            "model"), 1, GL_FALSE, glm::value_ptr(model));
                        drawObject(main_shader_program, minecraft_block,
                            minecraft_block_n_meshes);
                }
            }
        }

        // draw steve
        model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(1.0f, 0.15f, 2.0f));
        model = glm::scale(model, glm::vec3(0.05f, 0.05f, 0.05f));
        setMat4Uniform(main_shader_program, "model", model);
        drawObject(main_shader_program, steve, steve_n_meshes);

        // draw skybox
        glDepthFunc(GL_LEQUAL);
        glUseProgram(skybox_shader_program);
        view = glm::mat4(glm::mat3(view));
        model = glm::rotate(glm::mat4(1.0f), (float)glfwGetTime() * glm::radians(0.5f), glm::vec3(0.0f, 1.0f, 0.0f));
        setMat4Uniform(skybox_shader_program, "view", view);
        setMat4Uniform(skybox_shader_program, "projection", projection);
        setMat4Uniform(skybox_shader_program, "model", model);
        drawSkybox(skybox);
        glDepthFunc(GL_LESS);

        // swap back and front buffers
        glfwSwapBuffers(window);
    }

    // deallocate resources
    glfwTerminate();

    return 0;
}


/* ~~~ Aux functions ~~~ */

// Modify opengl viewport each time window is resized
void framebufferSizeCallback(GLFWwindow *window, int width, int height)
{
    glViewport(0, 0, width, height);
}


// Process keyboard input
void keyboardInput(GLFWwindow *window, float delta_time)
{
    // Close the game (ESC key)
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    // WASD user movement
    camera_pos += userWASDMovement(window, delta_time, camera_pos,
                                   camera_front, camera_up);
}


// Process mouse input
void mouseInputCallback(GLFWwindow* window, double x_pos, double y_pos)
{
    // Mouse movement
    camera_front = userMouseMovement(&first_mouse, &last_x, &last_y,
                                     x_pos, y_pos, &yaw, &pitch);
}


// Function to set mat4 uniform
void setMat4Uniform(unsigned int shader_program, const char *var_name,
                    glm::mat4 value)
{
    unsigned int loc = glGetUniformLocation(shader_program, var_name);
    glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(value));
}


// Function to set vet3 uniform
void setVec3Uniform(unsigned int shader_program, const char *var_name,
                    glm::vec3 value)
{
    unsigned int loc = glGetUniformLocation(shader_program, var_name);
    glUniform3fv(loc, 1, glm::value_ptr(value));
}


// Read csv map and draw it
void read_csv_map(FILE *map_file, unsigned int shader_program)
{

    // TODO: use map_file.csv or whatever instead of this
    // TODO: find a better way to visually create the map
    // TODO: random procedural map
    int width = 20, height = 20;
    char map[20][20][10] =
    {
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-T", "1-O", "1-O", "1-O", "1-O", "1-T", "1-O",
         "1-O", "1-O", "1-O", "1-T", "1-O", "1-O", "1-O", "1-O", "1-T", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-T", "1-O", "1-O", "1-O", "1-O", "1-T", "1-O",
         "1-O", "1-O", "1-O", "1-T", "1-O", "1-O", "1-O", "1-O", "1-T", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-C", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-C", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-C", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-T", "1-O", "1-O", "1-O", "1-O", "1-T", "1-O",
         "1-O", "1-O", "1-O", "1-T", "1-O", "1-O", "1-O", "1-O", "1-T", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"},
        {"1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O",
         "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O", "1-O"}
    };

    int x_pos, z_pos, y_pos, n_tiles;
    glm::mat4 model;

    // read csv and draw map
    for (x_pos = 0; x_pos < width; x_pos++) {
        for (z_pos = 0; z_pos < height; z_pos++) {

            if (map[x_pos][z_pos][0] >= '0' && map[x_pos][z_pos][0] <= '9')
            {
                n_tiles = (int)map[x_pos][z_pos][0] - 48;
                for (y_pos = 0; y_pos < n_tiles; y_pos++) {
                    model = glm::mat4(1.0f);
                    model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
                    model = glm::translate(model,
                        2.0f * glm::vec3(0.0f + x_pos, 0.0f + y_pos, 0.0f + z_pos));
                    glUniformMatrix4fv(glGetUniformLocation(shader_program,
                        "model"), 1, GL_FALSE, glm::value_ptr(model));
                    drawObject(shader_program, minecraft_block,
                        minecraft_block_n_meshes);
                }

                if (map[x_pos][z_pos][2] == 'C')
                {
                    model = glm::mat4(1.0f);
                    model = glm::scale(model, glm::vec3(0.1f, 0.1f, 0.1f));
                    model = glm::translate(model,
                        2.0f * glm::vec3(0.0f + x_pos, 0.0f + y_pos, 0.0f + z_pos));
                    glUniformMatrix4fv(glGetUniformLocation(shader_program,
                        "model"), 1, GL_FALSE, glm::value_ptr(model));
                    drawObject(shader_program, minecraft_cow,
                        minecraft_cow_n_meshes);
                }

                else if (map[x_pos][z_pos][2] == 'T')
                {
                    model = glm::mat4(1.0f);
                    model = glm::scale(model, glm::vec3(0.08f, 0.08f, 0.08f));
                    model = glm::translate(model,
                        2.5f * glm::vec3(0.0f + x_pos, 2.65f * (0.0f + n_tiles), 0.0f + z_pos));
                    glUniformMatrix4fv(glGetUniformLocation(shader_program,
                        "model"), 1, GL_FALSE, glm::value_ptr(model));
                    drawObject(shader_program, minecraft_tree,
                        minecraft_tree_n_meshes);
                }
            }
        }
    }
}
