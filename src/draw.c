/* Functions to draw loaded models and other stuff */

#include <climb3d/draw.h>


/* Draw skybox */
void drawSkybox(Skybox *skybox)
{
    glBindVertexArray(skybox->VAO);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, skybox->texture_id);
    // glDrawArrays(GL_TRIANGLES, 0, 36);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
}


/* Draw mesh */
void drawMesh(unsigned int shader_program, Mesh *mesh)
{
    unsigned int i, loc;

    for (i = 0; i < mesh->n_textures; i++)
    {
        // active the proper texture unit
        glActiveTexture(GL_TEXTURE0 + i);

        // diffuse texture
        if (!strcmp(mesh->textures[i].type, "diffuse"))
        {
            loc = glGetUniformLocation(shader_program, "texture_diffuse1");
            glUniform1i(loc, i);
        }
        // specular texture
        else if (!strcmp(mesh->textures[i].type, "specular"))
        {
            loc = glGetUniformLocation(shader_program, "texture_specular1");
            glUniform1i(loc, i);
        }
        // ambient texture
        else if (!strcmp(mesh->textures[i].type, "ambient"))
        {
            loc = glGetUniformLocation(shader_program, "texture_ambient1");
            glUniform1i(loc, i);
        }
        glBindTexture(GL_TEXTURE_2D, mesh->textures[i].id);
    }
    glActiveTexture(GL_TEXTURE0);

    // draw mesh
    glBindVertexArray(mesh->VAO);
    glDrawElements(GL_TRIANGLES, mesh->n_indices, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}


/* Draw an object */
void drawObject(unsigned int shader_program, Mesh *meshes,
                unsigned int n_meshes)
{
    unsigned int i;
    for (i = 0; i < n_meshes; i++)
        drawMesh(shader_program, &meshes[i]);
}
